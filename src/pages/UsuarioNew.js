import React, { Component } from 'react';

import Requests from '../services/Requests';
import PageLoading from '../components/PageLoading';
import { Container } from 'react-bootstrap';
import FormUsuario from '../components/FormUsuario';

class UsuarioNew extends Component {
  constructor(props) {
    super(props)
    this.Requests = new Requests();
  }

  state = { 
    loading: false,
    error: null,
    form: {
      rol:"Vendedor",
      nombre:"",
      tipo_documento:"",
      num_documento:"",
      direccion:"",
      telefono:"",
      email:"",
      password:"",
    }
  };

  handleChange = e => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      }
    })
  }

  handleSubmit = async e => {
    e.preventDefault()
    this.setState({loading: true, error: null});
    try {
      const path = '/usuario/add';
      await this.Requests.add(path,this.state.form);
      this.setState({loading: false});
      this.props.history.push('/usuarios');
    } catch (error) {
      this.setState({loading: false, error: error});
      console.log('ERR',error);
    }
  }

  render() {
    if (this.state.loading) {
      return <PageLoading />
    }
    return (
      <React.Fragment>
        <h1 className="mt-5">Nuevo Usuario</h1>
        <Container>
          <FormUsuario
            onChange={this.handleChange} 
            formValues={this.state.form}
            onSubmit={this.handleSubmit}
            error={this.state.error}
          />
        </Container>
      </React.Fragment>
    )
  }
}

export default UsuarioNew;