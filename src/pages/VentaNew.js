import React, { Component } from 'react';

import Requests from '../services/Requests';
import AuthService from '../services/AuthService';
import PageLoading from '../components/PageLoading';
import { Card } from 'react-bootstrap';
import FormVenta from '../components/FormVenta';

class VentaNew extends Component {
  constructor(props) {
    super(props)
    this.Requests = new Requests();
    this.AuthService = new AuthService();
  }

  state = { 
    loading: false,
    error: null,
    form: {
      usuario:"", //Llenado Fetch
      empresa:"", //Llenado Fetch
      dosificacion:"", //Llenado Fetch
      especificacion:"",  //LLENA AUTOMATICAMENTE
      fechaEmision:"",
      numeroFactura:"",  //LLENA AUTOMATICAMENTE
      numeroAutorizacion:"",  //LLENA AUTOMATICAMENTE
      estado:"",  //LLENA AUTOMATICAMENTE
      fsn:false,
      nitCliente:"",
      nombreRazonSocial:"",
      importeTotal:0,
      importeICE:0,  // NO SE NECESITA CAMBIAR
      exportaciones:0,  // NO SE NECESITA CAMBIAR
      ventasGravadas:0,  // NO SE NECESITA CAMBIAR
      subTotal:0, 
      descuento:0,
      importeBase:0,
      debitoFiscal:"",  //LLENA AUTOMATICAMENTE
      codigoControl:"",  //LLENA AUTOMATICAMENTE
      logoEmpresa:"",  //LLENA AUTOMATICAMENTE
      direccionEmpresa:"",  //LLENA AUTOMATICAMENTE
      fechaLimite:"",  //LLENA AUTOMATICAMENTE
      usuarioAnulador:"",
      fechaAnulacion:"",
      motivoAnulacion:"",
      codigoMetodoPago:"",
      numeroTarjeta:"0",
      leyenda:"",  //LLENA AUTOMATICAMENTE
      detalle:[]
    },
    touched: {
      fechaEmision: false,
      nitCliente: false,
    },
    user:{
      nombre:"",
    },
    empresa: {
      nombre:"",
      fecFin:Date(),
      fecUfactura:Date(),
    },
    dosificacion:{
      numeroSucursal:"",

    },
    productos:{},
    clientes:{}
  };

  handleChange = e => {
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    if(name === "fsn" && this.state.form.subTotal<3000){
      this.setState({
        form: {
          ...this.state.form,
          [name]: value,
          nitCliente: value?"90011":"",
          nombreRazonSocial: value?"CONSUMIDOR FINAL":"",
        }
      })
    }
    else if(name === "nitCliente" && this.state.form.fsn){
      this.setState({
        form: {
          ...this.state.form,
          fsn: false,
          [name]: value,
          nombreRazonSocial: "",
        }
      })
    }
    else{
      this.setState({
        form: {
          ...this.state.form,
          [name]: value,
        }
      })
    }
    if(name === "nitCliente" && !this.state.form.fsn){
      let clienteFilter = this.state.clientes.filter(cliente => cliente.nit === parseInt(value))
      if(clienteFilter.length !== 0){
        // console.log(clienteFilter)
        this.setState({
          form: {
            ...this.state.form,
            [name]: value,
            nombreRazonSocial:clienteFilter[0].nombre,
          }
        })
      }else{
        this.setState({
          form: {
            ...this.state.form,
            [name]: value,
            nombreRazonSocial:'',
          }
        })
      }
    }
  }

  handleSubmit = async e => {
    e.preventDefault()
    console.log("click submit");
    // this.setState({loading: true, error: null});
    // try {
    //   const path = '/usuario/add';
    //   await this.Requests.add(path,this.state.form);
    //   this.setState({loading: false});
    //   this.props.history.push('/usuarios');
    // } catch (error) {
    //   this.setState({loading: false, error: error});
    //   console.log('ERR',error);
    // }
  }

  handleBlur = (field) => (e) => {
    this.setState({
      touched:{
        ...this.state.touched,
        [field]:true
      }
    })
  }

  validate(fechaEmision, nitCliente) {
    const errors = {
      fechaEmision: '',
      nitCliente: '',
      nombreRazonSocial: '',
    }

    if(this.state.touched.fechaEmision && Date.parse(fechaEmision) < Date.parse(this.state.dosificacion.fechaUfactura)){
      errors.fechaEmision = 'Debe ser mayor o igual a la fecha anterior factura'
    }
    else if(this.state.touched.fechaEmision && Date.parse(fechaEmision) > Date.parse(this.state.dosificacion.fechaFin)){
      errors.fechaEmision = 'Debe ser menor o igual a la fecha limite emision'
    }

    if(this.state.touched.nitCliente && !this.state.form.fsn){
      let clienteFilter = this.state.clientes.filter(cliente => cliente.nit === parseInt(nitCliente))
      if(clienteFilter.length === 0){
        errors.nitCliente = 'No se encuentra ese cliente'
      }
    }
    return errors;
  }

  componentDidMount () {
    this.fetchData();
  }

  fetchData = async () => {
    this.setState({loading: true, error:null})
    try {
      const user = this.AuthService.getUser();
      const path1 = '/empresa/query?_id='+user.empresa;
      const empresa = await this.Requests.query(path1);
      const path2 = '/dosificacion/queryActivo';
      const dosificacion = await this.Requests.query(path2);
      const path3 = '/producto/list';
      const productos = await this.Requests.list(path3);
      const path4 = '/cliente/list';
      const clientes = await this.Requests.list(path4);

      if(dosificacion.message){
        alert(dosificacion.message);
        this.props.history.push('/ventas');
      }
      else{
        this.setState({empresa:empresa, dosificacion:dosificacion, user:user, productos:productos, clientes:clientes});
        this.setState({
          form:{
            ...this.state.form,
            usuario: user._id,
            empresa: empresa._id,
            dosificacion: dosificacion._id,
          }
        })
      }

      this.setState({loading: false});
    } catch(error) {
      this.setState({loading: false, error:error});
      console.log('ERR',error);
    }
  }

  addItem = (id)=>{
    if(id === "default")//Opcion por defecto de un select
      return
    // let indx = this.props.fields[property].options.findIndex(el=>el.id == id);
    let form = this.state.form;
    if(!Array.isArray(form.detalle))
      form.detalle = [];
    // if(form[detalle].findIndex(el=>el._id == id)==-1){
    //   //Solo agregamos un articulo que no este en la lista
    //   form[detalle].push({_id:id,articulo:this.props.fields[property].options[indx].value,precio:1,cantidad:1});
    // }
    form.detalle.push({_id:id})
    this.setState({form});
  }

  pushItem = (id)=>{
    if(id === "default")//Opcion por defecto de un select
      return
    let indx = this.state.productos.findIndex(e=>e._id === id);
    console.log("indx", indx);
    let form = this.state.form;
    if(!Array.isArray(form.detalle))
      form.detalle = [];
    if(form.detalle.findIndex(e=>e._id === id)===-1){
      //Solo agregamos un articulo que no este en la lista
      // form.detalle.push({_id:id,precio:1,cantidad:1});
      form.detalle.push({_id:id,codigo:this.state.productos[indx].codigo,nombre:this.state.productos[indx].nombre,precioVenta:this.state.productos[indx].precioVenta,cantidad:1});
    }
    this.setState({form});
  }

  changeItemContent(property,index,subproperty,value){
    let form = this.state.form;
    form[property][index][subproperty] = value;
    this.setState({form});
  }

  render() {
    if (this.state.loading) {
      return <PageLoading />
    }
    return (
      <React.Fragment>
        <h1 className="mt-5">Nueva Venta</h1>
        <Card className="mb-5">
          <FormVenta
            onChange={this.handleChange}
            formValues={this.state.form}
            onSubmit={this.handleSubmit}
            onBlur={this.handleBlur}
            errors={this.validate(this.state.form.fechaEmision, this.state.form.nitCliente)}
            error={this.state.error} 
            empresa={this.state.empresa} 
            dosificacion={this.state.dosificacion} 
            clientes={this.state.clientes}
            productos={this.state.productos}
            addItem={this.addItem}
            pushItem={this.pushItem}/>
        </Card>
      </React.Fragment>
    )
  }
    
}

export default VentaNew;