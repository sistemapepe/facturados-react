import React,{Component} from 'react';

import {Link} from 'react-router-dom';
import MiniLoader from '../components/MiniLoader';
import Requests from '../services/Requests';
import './styles/Pagina.css';
import {Container} from 'react-bootstrap';
import Table from '../components/Table';
import { Form,FormControl, Tab, Tabs } from 'react-bootstrap';
import ModalComponent from '../components/Modal';

class Productos extends Component{
  state = {
    loading: false,
    error: null,
    activeTab: "0",
    data: undefined,
    data2: undefined,
    filterText: '',
    row: null,
    showModalState:false,
    showModalRemove:false,
    titleModal:'',
    messageModal:'',
    ruta:['/producto','/categoria'],
    link:['productos/','categorias/'],
    objeto:['producto','categoria'],
    title:['Productos','Categorias'],
  }

  constructor(props){
    super(props);
    this.Requests = new Requests();
    this.columns = [
      [
        {name:"Codigo", selector:"codigo", sortable:true},
        {name:"Nombre", selector:"nombre", sortable:true},
        {name:"Precio", selector:"precioVenta", sortable:true},
        {name:"Unidad Medida", selector:"unidadMedida", sortable:true},
        {name:"Stock", selector:"stock", sortable:true},
      ],
      [
        {name:"Nombre", selector:"nombre", sortable:true},
        {name:"Descripcion", selector:"descripcion", sortable:true},
      ]
    ]
  }

  componentDidMount () {
    this.fetchData();

    // this.intervalId = setInterval(this.fetchData, 5000);
  }

  // componentWillMount() {
  //   clearInterval(this.intervalId)
  // }

  fetchData = async () => {
    this.setState({loading: true, error:null})
    try {
      const path = this.state.ruta[0]+'/list';
      const data = await this.Requests.list(path);
      const path2 = this.state.ruta[1]+'/list';
      const data2 = await this.Requests.list(path2);
      // console.log('datos',data);
      this.setState({loading: false, data:data, data2:data2})
    } catch(error) {
      this.setState({loading: false, error:error})
      console.log('ERR',error);
    }
  }

  handleChangeTab = (e) => {
    this.setState({activeTab : e});
    this.fetchData();
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleState = (row) =>{
    this.setState({row:row, titleModal:"Activar o Desactivar", messageModal:"¿Desea activar/desactivar este "+this.state.title[this.state.activeTab]+"?"});
    this.handleModalStateShow();
    
  }

  handleModalStateShow = e => {
    this.setState({showModalState:true})
  }

  handleModalStateClose = e => {
    this.setState({showModalState:false});
  }

  handleModalStateAccept = async e => {
    try {
      if(this.state.row.estado === 0) {
        await this.Requests.ActivateOrDeactivate(this.state.ruta[this.state.activeTab]+'/activate',this.state.row._id);
      }else {
        await this.Requests.ActivateOrDeactivate(this.state.ruta[this.state.activeTab]+'/deactivate',this.state.row._id);
      }
      this.setState({row:null, titleModal:"", messageModal:""});
      this.fetchData();
      this.handleModalStateClose();
    } catch (error) {
      console.log(error);
    }
  }

  handleRemove = (row) =>{
    this.setState({row:row, titleModal:"Eliminar "+this.state.objeto[this.state.activeTab], messageModal:"¿Desea eliminar este "+this.state.objeto[this.state.activeTab]+"?"});
    this.handleModalRemoveShow();
    
  }

  handleModalRemoveShow = e => {
    this.setState({showModalRemove:true})
  }

  handleModalRemoveClose = e => {
    this.setState({showModalRemove:false});
  }

  handleModalRemoveAccept = async e => {
    try {
      await this.Requests.delete(this.state.ruta[this.state.activeTab]+'/remove',this.state.row._id);
      this.setState({row:null, titleModal:"", messageModal:""});
      this.fetchData();
      this.handleModalRemoveClose();
    } catch (error) {
      console.log(error);
    }
  }

  render(){
    let tab = this.state.data;
    let tab2 = this.state.data2;
    let filteredItems = [tab,tab2];
    if (tab!==undefined) {
      filteredItems = [tab.filter(producto => producto.nombre && producto.nombre.toLowerCase().includes(this.state.filterText.toLowerCase())),tab2.filter(categoria => categoria.nombre && categoria.nombre.toLowerCase().includes(this.state.filterText.toLowerCase()))];
    }

    return (
      <React.Fragment>
        <h1 className="mt-5">{this.state.title[this.state.activeTab]}</h1>
        <Tabs value={this.state.activeTab} onSelect={this.handleChangeTab} className="mb-5">
          <Tab eventKey={0} title="Productos"></Tab>
          <Tab eventKey={1} title="Categorias"></Tab>
        </Tabs>
        {this.state.activeTab === "0" &&
          (<Container>
            <div className="new__buttons">
              <Link to={this.state.link[this.state.activeTab]+"new"} className="btn btn-primary">{"Nuevo "+this.state.objeto[this.state.activeTab]}</Link>
            </div>
            <div>
              <Form inline className="new__buttons">
                <FormControl type="text" placeholder="Buscar" name="filterText" onChange={this.handleChange} value={this.state.filterText} />
              </Form>
            </div>  
            <ModalComponent show={this.state.showModalState} handleClose={this.handleModalStateClose} handleAccept={this.handleModalStateAccept} title={this.state.titleModal} message={this.state.messageModal}></ModalComponent>
            <ModalComponent show={this.state.showModalRemove} handleClose={this.handleModalRemoveClose} handleAccept={this.handleModalRemoveAccept} title={this.state.titleModal} message={this.state.messageModal}></ModalComponent>     
            <Table 
              columns={this.columns[this.state.activeTab]} 
              data={filteredItems[this.state.activeTab]} 
              changeState={this.handleState} 
              handleRemove={this.handleRemove}
              dir={this.state.link[this.state.activeTab]}>
            </Table>
            {this.state.loading && <MiniLoader/>}
          </Container>)

        }
        {this.state.activeTab === "1" &&
          (<Container>
            <div className="new__buttons">
              <Link to={this.state.link[this.state.activeTab]+"new"} className="btn btn-primary">{"Nuevo "+this.state.objeto[this.state.activeTab]}</Link>
            </div>
            <div>
              <Form inline className="new__buttons">
                <FormControl type="text" placeholder="Buscar" name="filterText" onChange={this.handleChange} value={this.state.filterText} />
              </Form>
            </div>  
            <ModalComponent show={this.state.showModalState} handleClose={this.handleModalStateClose} handleAccept={this.handleModalStateAccept} title={this.state.titleModal} message={this.state.messageModal}></ModalComponent>
            <ModalComponent show={this.state.showModalRemove} handleClose={this.handleModalRemoveClose} handleAccept={this.handleModalRemoveAccept} title={this.state.titleModal} message={this.state.messageModal}></ModalComponent>     
            <Table 
              columns={this.columns[this.state.activeTab]} 
              data={filteredItems[this.state.activeTab]} 
              changeState={this.handleState} 
              handleRemove={this.handleRemove}
              dir={this.state.link[this.state.activeTab]}>
            </Table>
            {this.state.loading && <MiniLoader/>}
          </Container>)

        }

        
      </React.Fragment>
    )
  }
}

export default Productos;