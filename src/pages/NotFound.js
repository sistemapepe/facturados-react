import React from 'react';

function NotFound() {
    return (
        <h1 className="mt-5">404: NotFound</h1>
    );
}

export default NotFound;