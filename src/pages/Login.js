import React, { Component } from 'react'

import AuthService from '../services/AuthService';
import './styles/Login.css';
import logo from '../images/logo.png';
import PageLoading from '../components/PageLoading';

import {Image, Button, Form} from 'react-bootstrap';

class Login extends Component {
  constructor(props){
    super(props);
    this.state = {
      loading: false,
      error: null,
      form: {
        email: '',
        password: '',
      }
    };
    
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    
    this.authService = new AuthService();
  }

  handleChange = e => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]:e.target.value,
      }
    });
  }

  handleSubmit = async e => {
    e.preventDefault();
    this.setState({loading:true, error:null});
    try {
      await this.authService.login(this.state.form.email,this.state.form.password);
      this.setState({loading:false});
      this.props.onAuthChange();
      this.props.history.replace('/');
    } catch (error) {
      this.setState({loading:false, error:error.message});
      console.log("ERRORR",error.message);
    }
  }

  render() {
    if (this.state.loading) {
      return <PageLoading />
    }
    return (
      <div className="text-center body">
        <Form className="form-signin" onSubmit={this.handleSubmit}>
          <Image className="mb-4" src={logo} thumbnail/>
          <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1>
          <Form.Group controlId="inputEmail">
            <Form.Label className="sr-only">Email address</Form.Label>
            <Form.Control type="email" className="form-control" placeholder="Email address" required autoFocus name="email" onChange={this.handleChange}/>  
          </Form.Group>
          <Form.Group controlId="inputPassword">
            <Form.Label className="sr-only">Password</Form.Label>
            <Form.Control type="password" className="form-control" placeholder="Password" required name="password" onChange={this.handleChange}/>
           </Form.Group>
          <Button size="lg" variant="dark" block type="submit">Sign in</Button>
          {this.props.error && (
            <p className="text-danger">{this.state.error}</p>
          )}
        </Form>
        <p className="mt-2 mb-3 text-muted">&copy; JSM SOLUTIONS</p>
      </div>
    )
  }
}

export default Login;