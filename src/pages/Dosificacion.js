import React,{Component} from 'react';

import {Link} from 'react-router-dom';
import MiniLoader from '../components/MiniLoader';
import Requests from '../services/Requests';
import './styles/Pagina.css';
import {Container} from 'react-bootstrap';
import Table from '../components/Table';
import { Form,FormControl} from 'react-bootstrap';
import ModalComponent from '../components/Modal';

class Dosificaciones extends Component{
  state = {
    loading: false,
    error: null,
    data: undefined,
    filterText: '',
    row: null,
    showModalState:false,
    showModalRemove:false,
    titleModal:'',
    messageModal:'',
    ruta:'/dosificacion',
    link:'dosificacion/',
    objeto:'dosificacion',
    title:'Dosificacion',
  }

  constructor(props){
    super(props);
    this.Requests = new Requests();
    this.columns = [
      {name:"Tramite", selector:"nroTramite", sortable:true},
      {name:"Nro Autorizacion", selector:"nroAutorizacion", sortable:true},
      {name:"Inicio",
      cell:(row) =>{
        return(
          <>
          {/* <span>{new Intl.DateTimeFormat('es-ES', { year: 'numeric', month: '2-digit', day: '2-digit'}).format(new Date(Date.parse(row.fechaInicio)))}</span> */}
          <span>{new Intl.DateTimeFormat('en-GB').format(new Date(row.fechaInicio))}</span>
          </>
        )
      }, sortable:true},
      {name:"Fin",
      cell:(row) =>{
        return(
          <span>{new Intl.DateTimeFormat('es-ES', { year: 'numeric', month: '2-digit', day: '2-digit'}).format(new Date(Date.parse(row.fechaFin)))}</span>
        )
      }, sortable:true},
      {name:"Tipo", selector:"tipoFactura", sortable:true},
    ]
  }

  componentDidMount () {
    this.fetchData();

    // this.intervalId = setInterval(this.fetchData, 5000);
  }

  // componentWillMount() {
  //   clearInterval(this.intervalId)
  // }

  fetchData = async () => {
    this.setState({loading: true, error:null})
    try {
      const path = this.state.ruta+'/list';
      const data = await this.Requests.list(path);
      console.log('datos',data);
      this.setState({loading: false, data:data})
    } catch(error) {
      this.setState({loading: false, error:error})
      console.log('ERR',error);
    }
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleState = (row) =>{
    this.setState({row:row, titleModal:"Activar o Desactivar", messageModal:"¿Desea activar/desactivar este "+this.state.title+"?"});
    this.handleModalStateShow();
    
  }

  handleModalStateShow = e => {
    this.setState({showModalState:true})
  }

  handleModalStateClose = e => {
    this.setState({showModalState:false});
  }

  handleModalStateAccept = async e => {
    try {
      if(this.state.row.estado === 0) {
        await this.Requests.ActivateOrDeactivate(this.state.ruta+'/activate',this.state.row._id);
      }else {
        await this.Requests.ActivateOrDeactivate(this.state.ruta+'/deactivate',this.state.row._id);
      }
      this.setState({row:null, titleModal:"", messageModal:""});
      this.fetchData();
      this.handleModalStateClose();
    } catch (error) {
      console.log(error);
    }
  }

  handleRemove = (row) =>{
    this.setState({row:row, titleModal:"Eliminar "+this.state.objeto, messageModal:"¿Desea eliminar este "+this.state.objeto+"?"});
    this.handleModalRemoveShow();
    
  }

  handleModalRemoveShow = e => {
    this.setState({showModalRemove:true})
  }

  handleModalRemoveClose = e => {
    this.setState({showModalRemove:false});
  }

  handleModalRemoveAccept = async e => {
    try {
      await this.Requests.delete(this.state.ruta+'/remove',this.state.row._id);
      this.setState({row:null, titleModal:"", messageModal:""});
      this.fetchData();
      this.handleModalRemoveClose();
    } catch (error) {
      console.log(error);
    }
  }

  render(){
    let tab = this.state.data;
    let filteredItems = tab;
    if (tab!==undefined) {
      filteredItems = tab.filter(dosificacion => dosificacion.nroAutorizacion.toString().includes(this.state.filterText.toLowerCase()));
    }

    return (
      <React.Fragment>
        <h1 className="mt-5">{this.state.title}</h1>
        <Container>
          <div className="new__buttons">
            <Link to={this.state.link+"new"} className="btn btn-primary">{"Nuevo "+this.state.objeto}</Link>
          </div>
          <div>
            <Form inline className="new__buttons">
              <FormControl type="text" placeholder="Buscar" name="filterText" onChange={this.handleChange} value={this.state.filterText} />
            </Form>
          </div>  
          <ModalComponent show={this.state.showModalState} handleClose={this.handleModalStateClose} handleAccept={this.handleModalStateAccept} title={this.state.titleModal} message={this.state.messageModal}></ModalComponent>
          <ModalComponent show={this.state.showModalRemove} handleClose={this.handleModalRemoveClose} handleAccept={this.handleModalRemoveAccept} title={this.state.titleModal} message={this.state.messageModal}></ModalComponent>     
          <Table 
            columns={this.columns} 
            data={filteredItems} 
            changeState={this.handleState} 
            handleRemove={this.handleRemove}
            dir={this.state.link}>
          </Table>
          {this.state.loading && <MiniLoader/>}
        </Container>
      </React.Fragment>
    )
  }
}

export default Dosificaciones;