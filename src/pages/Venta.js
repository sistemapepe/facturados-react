import React, { Component } from 'react';

import Requests from '../services/Requests';
import PageLoading from '../components/PageLoading';
import { Form, Col, Row } from 'react-bootstrap';

class Venta extends Component {
  constructor(props) {
    super(props)
    this.Requests = new Requests();
  }

  state = { 
    loading: false,
    error: null,
    form: {
      vendedor:[],
      empresa:[],
      dosificacion:[],
      //DATOS FACTURA
      nroAutorizacion:"",
      nroFactura:"",
      fecha:"",
      nit:"",
      nombre:"",
      total:"",
      codigoControl:"",
      detalle:[],
      //EXTRAS
      no_iva_ice:"",
      exporta:"",
      subTotal:"",
      descuento:"",
      importe_neto:"",
      iva_df:"",   
      //CONTROL
      estado:"",
      anulador:"",
      fecAnulacion:"",
    },
  };

  componentDidMount() {
    this.fetchData()
  }

  fetchData = async e => {
    this.setState({loading: true, error: null})
    try {
      const path = '/venta/query?_id='+this.props.match.params._id;
      const data = await this.Requests.query(path);
      console.log(data);
      this.setState({loading: false, form: data});
    } catch (error) {
      this.setState({loading: false, error: error});
    }
  }

  handleChange = e => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      }
    })
  }

  render() {
    const venta = this.state.form;
    if (this.state.loading) {
      return <PageLoading />
    }
    return (
      <React.Fragment>
        <h1 className="mt-5">Venta Nº{venta.nroFactura}</h1>
        <Form>
          <Row>
            <Col className="col-12 col-md-4">
              <Form.Group controlId="Usuario">
                <Form.Label>Usuario</Form.Label>
                <Form.Control type="text" name="usuario"  />
              </Form.Group>

              <Form.Row>
                <Form.Group as={Col} controlId="ClienteNit">
                  <Form.Label>Cliente</Form.Label>
                  <Form.Control type="number" placeholder="Nit" name="nit" onChange={this.props.onChange} />
                </Form.Group>
      
                <Form.Group as={Col} controlId="ClienteNombre">
                  <Form.Label></Form.Label>
                  <Form.Control type="text" placeholder="Tipo" name="nombre" onChange={this.props.onChange} />
                </Form.Group>
              </Form.Row>
      
      
              <Form.Group controlId="direccion">
                <Form.Label>Direccion</Form.Label>
                <Form.Control type="text" placeholder="Direccion" name="direccion" onChange={this.props.onChange} />
              </Form.Group>
      
              <Form.Group controlId="Telefono">
                <Form.Label>Telefono</Form.Label>
                <Form.Control type="tel" placeholder="Numero de telefono" name="telefono" onChange={this.props.onChange} />
              </Form.Group>
      
              <Form.Group controlId="Rol">
                <Form.Label>Rol</Form.Label>
                <Form.Control as="select" name="rol" onChange={this.props.onChange} >
                  <option>Almacenero</option>
                  <option>Vendedor</option>
                  <option>Administrador</option>
                </Form.Control>
              </Form.Group>
            
            </Col>
            <Col className="col-12 col-md-8"></Col>
          </Row>
        </Form>
      </React.Fragment>
    )
  }
}

export default Venta