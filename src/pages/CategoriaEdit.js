import React, { Component } from 'react';

import Requests from '../services/Requests';
import { Container } from 'react-bootstrap';
import PageLoading from '../components/PageLoading';
import FormCategoria from '../components/FormCategoria';

class CategoriaEdit extends Component {
  constructor(props) {
    super(props)
    this.Requests = new Requests();
  }

  state = { 
    loading: false,
    error: null,
    form: {
      nombre: "",
      descripcion: "",
    }
  };

  componentDidMount() {
    this.fetchData()
  }

  fetchData = async e => {
    this.setState({loading: true, error: null})
      try {
        const path = '/categoria/query?_id='+this.props.match.params._id;
        console.log(path);
        const data = await this.Requests.query(path);
        this.setState({loading: false, form: data});
      } catch (error) {
        this.setState({loading: false, error: error});
      }
  }

  handleChange = e => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      }
    })
  }

  handleSubmit = async e => {
    e.preventDefault()
    this.setState({loading: true, error: null});
    try {
      const path = '/categoria/update'
      await this.Requests.update(path, this.state.form);
      this.setState({loading: false});
      this.props.history.push('/productos')
    } catch (error) {
      this.setState({loading: false, error: error});
    }
  }

  render(){
    if (this.state.loading) {
      return <PageLoading />
    }

    return (
      <React.Fragment>
        <h1 className="mt-5">Editar Categoria</h1>
        <Container>
          <FormCategoria
            onChange={this.handleChange} 
            formValues={this.state.form}
            onSubmit={this.handleSubmit}
            error={this.state.error}
          />
        </Container>
      </React.Fragment>
    )
  }
}

export default CategoriaEdit;