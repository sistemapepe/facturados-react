import React, { Component } from 'react';
import './App.css';
import AuthService from './services/AuthService';
import Login from './pages/Login';
import {BrowserRouter,Route,Switch,Redirect} from 'react-router-dom';

import Main from './components/Main';

class App extends Component {

  constructor(props){
    super(props);
    this.auth = new AuthService();
    this.state = {auth: this.auth.isLoggedIn()};
    this.onAuthChange = this.onAuthChange.bind(this);
  }

  render() {
    if(this.state.auth){
      return(
        <Main onAuthChange={this.onAuthChange}/>
      );
    }
    return (
    <BrowserRouter>
      <Switch>
        <Route path="/Login" render={(routerProps)=> <Login {...routerProps} onAuthChange={this.onAuthChange}></Login>}/>
        <Redirect path="*" to="/login"/>
      </Switch>
    </BrowserRouter>
    );
  }

  onAuthChange(){
    this.setState({auth:this.auth.isLoggedIn()});
  }

}

export default App;
