import React, { Component } from 'react';

import { Form, Col, Button, InputGroup, Table} from 'react-bootstrap';
import {PersonPlusFill, Dash, Plus} from 'react-bootstrap-icons';
import Requests from '../services/Requests';
import './styles/FormVenta.css';
import Select from 'react-select';

class FormVenta extends Component {
  constructor(props) {
    super(props)
    this.state = {

    }
    this.Requests = new Requests();
  }

  render() {
    const list_productos = [];
    if(this.props.productos.length>0){
      let prod = this.props.productos;
      for(let i in prod){
        // console.log(i)
        list_productos.push({value:prod[i]._id, label:prod[i].codigo + " | " +prod[i].nombre});
      };
    }
    return (
      <Form onSubmit={this.props.onSubmit} className="m-3">
        <Form.Row>
          <Form.Group as={Col} xs="12" sm="6" controlId="Fecha" validationstate="error">
            <Form.Label>Fecha</Form.Label>
              <Form.Control 
                size="sm" type="date" 
                name="fechaEmision" required
                onChange={this.props.onChange} 
                value={this.props.formValues.fechaEmision}
                onBlur={this.props.onBlur('fechaEmision')}
                  
                isInvalid={this.props.errors.fechaEmision !== ''}
              />
            <Form.Control.Feedback type="invalid">{this.props.errors.fechaEmision}</Form.Control.Feedback>
          </Form.Group>

          <Form.Group as={Col} xs="6" sm="3" controlId="Empresa">
            <Form.Label>Empresa</Form.Label>
            <Form.Control size="sm" value={this.props.empresa.nombre} readOnly/>
          </Form.Group>

          <Form.Group as={Col} xs="6" sm="3" controlId="Sucursal">
            <Form.Label>Sucursal</Form.Label>
            <Form.Control size="sm" value={this.props.dosificacion.nroSucursal===0?'Casa Matriz':this.props.nroSucursal} readOnly/>
          </Form.Group>
        </Form.Row>
            
        <hr className="hr__separador"/>
            
        <Form.Row>
          <Form.Group as={Col} xs="12" sm="6" controlId="Nit">
            <Form.Label size="sm">NIT/CI</Form.Label>
            <InputGroup>
              <Form.Control 
                type="number" placeholder="Ejm. 19062020" 
                className="number__nit" name="nitCliente" 
                onChange={this.props.onChange}
                value={this.props.formValues.nitCliente}

                onBlur={this.props.onBlur('nitCliente')}
                isInvalid={this.props.errors.nitCliente !== ''}
              />
              
              <Button variant="outline-dark"><PersonPlusFill/></Button>
            </InputGroup>
                
          </Form.Group>
              
          <Form.Group as={Col} xs="12" sm="6" controlId="formBasicCheckbox" className="center__child">
            <Form.Check 
              type="checkbox" label="Factura sin nombre" 
              name="fsn" onChange={this.props.onChange} 
              checked={this.props.formValues.fsn} />
          </Form.Group>
        </Form.Row>

        <Form.Row>      
          <Form.Group as={Col} xs="12" sm="6" controlId="formGridState">
            <Form.Label size="sm">Señor(es)</Form.Label>
            <InputGroup>
              <Form.Control 
                type="text" placeholder="Ejm. Quispe" name="nombreRazonSocial" 
                onChange={this.props.onChange} 
                value={this.props.formValues.nombreRazonSocial} 
                // onBlur={this.props.onBlur('nombreRazonSocial')}
                // isInvalid={this.props.errors.nitCliente !== ''} 
                readOnly/>
              {/* <Form.Control.Feedback type="invalid" >{this.props.errors.nitCliente}</Form.Control.Feedback> */}
            </InputGroup>
          </Form.Group>

          <Form.Group as={Col} xs="12" sm="6" controlId="formGridState">
            <Form.Label size="sm">Email</Form.Label>
            <Form.Control type="email" placeholder="email@example.com" />
          </Form.Group>
        </Form.Row>
            
        <hr className="hr__separador"/>
        {/* <Button variant="dark" size="sm" className="ml-2" onClick={(ev)=>this.props.addItem(ev.target.value)}><Plus/> Agregar item</Button> */}
        <Select 
          className="input__codigo col-12 col-sm-6" isClearable={true} options={list_productos} 
          onChange={(e)=>{console.log(e);this.props.pushItem(e?e.value:"default")}}/>
        {/* this.props.productos.map(e => ({id:e.id, value:e._id, label:e.nombre})) */}
        <Table responsive="md" className="m-2 tabla__ventas">              
          <thead>
            <tr>
              <th className="td__cantidad">CANTIDAD</th> 
              <th className="td__codigo">CÓDIGO PRODUCTO</th>
              <th className="td__concepto">CONCEPTO</th>
              <th className="td__monto">PRECIO en Bs</th>
              <th className="td__monto">SUBTOTAL</th>
              <th className="td__descuento">DESCUENTO</th>
              <th className="td__monto">TOTAL A COBRAR</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {this.props.formValues.detalle.map((e,i)=>(
              <tr>
                <td><input type="number" min="1" defaultValue="1"/></td>
                <td><input type="number" min="1" value={e.codigo}/></td>
                <td><input type="text" value={e.nombre}/></td>
                <td><input type="number" min="0" className="input__money" value={e.precioVenta}/></td>
                <td><input type="number" min="0" className="input__money" placeholder="0,00"/></td>
                <td><input type="number" min="0" className="input__money" placeholder="0,00"/></td>
                <td><input type="number" min="0" className="input__money" placeholder="0,00"/></td>
                <td><Button variant="danger" size="sm"><Dash/></Button></td>
              </tr>
            ))}
          </tbody>
          <tfoot>
            <tr>
              <td colSpan="4" className="input__money">TOTALES</td>
              <td><input type="number" className="input__money" defaultValue="0.00" readOnly/></td>
              <td><input type="number" className="input__money" defaultValue="0.00" readOnly/></td>
              <td><input type="number" className="input__money" defaultValue="0.00" readOnly/></td>
              <td></td>
            </tr>
          </tfoot>
        </Table>

        <hr className="hr__separador"/>
        <Button variant="primary" type="submit">Guardar</Button>
      </Form>
    )
  }
}

export default FormVenta;