import React, { Component } from 'react';

import {Form, Button } from 'react-bootstrap';

class FormCategoria extends Component {
  constructor(props) {
    super(props)
    this.state = {
            
    }
  }

  handleClick(){
    console.log('click');
  }

  render() {
    return (
      <Form onSubmit={this.props.onSubmit} className="col-12 col-md-6 mb-5">
        <Form.Group controlId="categoria">
          <Form.Label>Nombre</Form.Label>
          <Form.Control type="text" placeholder="Ingresa Nombre" name="nombre" onChange={this.props.onChange} value={this.props.formValues.nombre} />
        </Form.Group>
          
        <Form.Group controlId="Descripcion">
          <Form.Label>Descripcion</Form.Label>
          <Form.Control type="text" placeholder="Ingrese Descripcion" name="descripcion" onChange={this.props.onChange} value={this.props.formValues.descripcion} />
        </Form.Group>

        <Button variant="primary" type="submit" onClick={this.handleClick}>
          Guardar
        </Button>
        {this.props.error && (
          <p className="text-danger">{this.props.error.message}</p>
        )}
      </Form>
    )
  }
}

export default FormCategoria;