import React, { Component } from 'react';

import { Button,Modal} from 'react-bootstrap';

class ModalComponent extends Component {
  render() {
    return (
      <Modal show={this.props.show} onHide={this.props.handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{this.props.title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{this.props.message}</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.props.handleClose}>
            Cerrar
          </Button>
          <Button variant="primary" onClick={this.props.handleAccept}>
            Aceptar
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default ModalComponent;