import React, { Component } from 'react';

import DataTable from 'react-data-table-component';
import {Pencil,TrashFill,FileEarmarkArrowDown} from 'react-bootstrap-icons';
import { Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';

class   Table extends Component {
  constructor(props) {
    super(props)
    this.state = {
      resetPaginationToggle:false,
      columns:[
        ...this.props.columns,
        {
          name:"Estado",
          cell:(row) =>{
            if(row.estado===0 && this.props.anulado){
              return(
                <div>
                  <Button variant='danger' disabled size="sm" block onClick={()=>this.props.changeState(row)}>Anulado</Button>
                </div>
              )
            }
            return(
              <div>
                <Button variant={row.estado===0?'danger':'success'} size="sm" block onClick={()=>this.props.changeState(row)}>
                  {row.estado===0?'Desactivo':'Activo'}
                </Button>
              </div>
            )
          },
          ignoreRowClick: true,
          allowOverflow: true,
          button: true,
        },
        {
          name:"Acciones",
          cell:(row) => {
            if(this.props.print){
              return(
                <div>
                  <Button variant="warning" size="sm"><Link to={this.props.dir+row._id+'/edit'}><Pencil color="white"/></Link></Button>
                  {' '}
                  <Button variant="danger" size="sm" onClick={()=>this.props.handleRemove(row)}><TrashFill color="white"/></Button>
                  {' '}
                  <Button variant="primary" size="sm"><Link to={this.props.dir+row._id+'/factura'}><FileEarmarkArrowDown color="white"/></Link></Button>
                </div>
              ) 
            }else {
              return(
                <div>
                  <Button variant="warning" size="sm"><Link to={this.props.dir+row._id+'/edit'}><Pencil color="white"/></Link></Button>
                  {' '}
                  <Button variant="danger" size="sm" onClick={()=>this.props.handleRemove(row)}><TrashFill color="white"/></Button>                           
                </div>
              ) 
            }
          },
          ignoreRowClick: true,
          allowOverflow: true,
          button: true,
        },
      ]
    }
  }

  render() {
    // PAGINACION
    const paginations = { 
      rowsPerPageText: 'Mostrar', 
      rangeSeparatorText: 'de', 
      noRowsPerPage: false, 
      selectAllRowsItem: false, 
      selectAllRowsItemText: 'todas' 
    }
    // console.log("datt",this.props.data);
    if(this.props.data && this.props.data.length !==0){
      return(
        <DataTable
          // title="Productos"
          // striped
          //theme={'dark'}
          columns={this.state.columns}
          data={this.props.data}
          noHeader={true}
          // subHeader
          // subHeaderComponent={this.buscador()}
          // data={filteredItems}
          // defaultSortField="title"
          // fixedHeader
          // fixedHeaderScrollHeight="300px"
          //selectableRows
          //onSelectedRowsChange={(ev)=>{this.mostrar(ev)}}
          // actions={actions}
          //clearSelectedRows={false}
          noContextMenu={true}
          //pagination
          pagination
          paginationResetDefaultPage={this.state.resetPaginationToggle}
          paginationComponentOptions={paginations}
        />
      );
    }else {
      return(
        <div>
          <h3>Sin registros</h3>
        </div>
      );
    }
  }
}

export default Table