import React, { PureComponent } from 'react';

import {Table, Button} from 'react-bootstrap';
import { Pencil, Trash } from 'react-bootstrap-icons';

class Tabla extends PureComponent {
  render() {
    if(this.props.data && this.props.data.length !==0){
      return(
        <div>
          <Table hover responsive>
            <thead>
              <tr>
              {this.props.header.map(headerTitle => (
                <th key={'h_'+headerTitle}>{headerTitle}</th>
              ))}
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              {this.props.data.map(row => (
                <tr key={'r_'+row._id}>
                  {this.props.ids.map((keyName,indR)=>(
                    <td key={'rc_'+keyName+indR}>
                      {keyName==='estado'?row[keyName]===1?(<Button variant="success">Activo</Button>):(<Button variant="danger">desactivo</Button>):keyName==='fecha'?row[keyName]:row[keyName]}
                    </td>
                  ))}
                    <td>
                    <Button variant="warning"><Pencil></Pencil></Button><Button variant="danger"><Trash></Trash></Button>
                    </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>        
      )
    }
    return (
      <div>
        <h3>Sin registros</h3>
      </div>
    )
  }
}


export default Tabla