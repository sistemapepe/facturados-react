import React, { Component } from 'react';

import {Form, Button, Col } from 'react-bootstrap';

class FormDosificacion extends Component {
  constructor(props) {
    super(props)
    this.state = {
            
    }
  }

  handleClick(){
    console.log('click');
  }

  render() {
    return (
      <Form onSubmit={this.props.onSubmit} className="col-12 col-md-6 mb-5">
        <Form.Group controlId="FechaTramite">
          <Form.Label>Fecha Tramite</Form.Label>
          <Form.Control type="date" name="fechaTramite" onChange={this.props.onChange} value={this.props.formValues.fechaTramite.substr(0,10)} />
        </Form.Group>
          
        <Form.Group controlId="NroTramite">
          <Form.Label>Numero Tramite</Form.Label>
          <Form.Control type="number" placeholder="Ingrese numero de tramite" name="nroTramite" onChange={this.props.onChange} value={this.props.formValues.nroTramite} />
        </Form.Group>

        <Form.Group controlId="NroSucursal">
          <Form.Label>Nro Sucursal</Form.Label>
          <Form.Control as="select" name="nroSucursal" onChange={this.props.onChange} value={this.props.formValues.nroSucursal}>
            <option value="0" >Casa Matriz</option>
          </Form.Control>
        </Form.Group>

        <Form.Group controlId="NroAutorizacion">
          <Form.Label>Numero Autorizacon</Form.Label>
          <Form.Control type="number" placeholder="Ingrese numero de autorizacion" name="nroAutorizacion" onChange={this.props.onChange} value={this.props.formValues.nroAutorizacion} />
        </Form.Group>

        <Form.Group controlId="TipoFactura">
          <Form.Label>Tipo Factura</Form.Label>
          <Form.Control as="select" name="tipoFactura" onChange={this.props.onChange} value={this.props.formValues.tipoFactura}>
            <option>COMPUTARIZADO</option>
          </Form.Control>
        </Form.Group>

        <Form.Row>
          
          <Form.Group as={Col} controlId="FechaInicio">
            <Form.Label>Fecha Inicio</Form.Label>
            <Form.Control type="date" name="fechaInicio" onChange={this.props.onChange} value={this.props.formValues.fechaInicio.substr(0,10)} />
          </Form.Group>

          <Form.Group as={Col} controlId="FechaFin">
            <Form.Label>Fecha Fin</Form.Label>
            <Form.Control type="date" name="fechaFin" onChange={this.props.onChange} value={this.props.formValues.fechaFin.substr(0,10)} />
          </Form.Group>
        
        </Form.Row>

        <Form.Group controlId="Llave">
          <Form.Label>Llave</Form.Label>
          <Form.Control type="text" placeholder="Ingrese la llave de dosificacion" name="llave" onChange={this.props.onChange} value={this.props.formValues.llave} />
        </Form.Group>

        <Form.Group controlId="Leyenda">
          <Form.Label>Leyenda</Form.Label>
          <Form.Control type="text" placeholder="Ingrese la leyenda" name="leyenda" onChange={this.props.onChange} value={this.props.formValues.leyenda} />
        </Form.Group>

        <Form.Row>
          
          <Form.Group as={Col} controlId="NroFacturas">
            <Form.Label>Numero Facturas</Form.Label>
            <Form.Control type="number" name="nroFacturas" disabled value={this.props.formValues.nroFacturas} />
          </Form.Group>

          <Form.Group as={Col} controlId="FechaUFactura">
            <Form.Label>Fecha Ultima Factura</Form.Label>
            <Form.Control type="date" name="fechaUfactura" disabled value={this.props.formValues.fechaUfactura.substr(0,10)} />
          </Form.Group>
        
        </Form.Row>

        <Form.Group controlId="TipoImpresion">
          <Form.Label>Tipo Impresion</Form.Label>
          <Form.Control as="select" name="tipoImpresion" onChange={this.props.onChange} value={this.props.formValues.tipoImpresion}>
            <option value="CARTA">CARTA</option>
            <option value="MOFICIO">MEDIO OFICIO</option>
          </Form.Control>
        </Form.Group>
        
          <Button variant="primary" type="submit" onClick={this.handleClick}>
          Guardar
        </Button>
        {this.props.error && (
          <p className="text-danger">{this.props.error.message}</p>
        )}
      </Form>
    )
  }
}

export default FormDosificacion;