import React, { Component } from 'react';

import {Form, Button, Col } from 'react-bootstrap';

class FormCliente extends Component {
  constructor(props) {
    super(props)
    this.state = {
            
    }
  }

  handleClick(){
    console.log('click');
  }

  render() {
    return (
      <Form onSubmit={this.props.onSubmit} className="col-12 col-md-6 mb-5">    
        <Form.Group controlId="FullName">
          <Form.Label>Nombre</Form.Label>
          <Form.Control type="text" placeholder="Ingrese Nombre" name="nombre" onChange={this.props.onChange} value={this.props.formValues.nombre} />
        </Form.Group>

        <Form.Row>
          <Form.Group as={Col} controlId="num_documento">
            <Form.Label>CI</Form.Label>
            <Form.Control type="number" placeholder="Numero" name="num_documento" onChange={this.props.onChange} value={this.props.formValues.num_documento}/>
          </Form.Group>

          <Form.Group as={Col} controlId="tipo_documento">
            <Form.Label>Extencion</Form.Label>
            <Form.Control type="text" placeholder="Tipo" name="tipo_documento" onChange={this.props.onChange} value={this.props.formValues.tipo_documento} />
          </Form.Group>
        </Form.Row>


        <Form.Group controlId="direccion">
          <Form.Label>Direccion</Form.Label>
          <Form.Control type="text" placeholder="Direccion" name="direccion" onChange={this.props.onChange} value={this.props.formValues.direccion} />
        </Form.Group>

        <Form.Group controlId="Telefono">
          <Form.Label>Telefono</Form.Label>
          <Form.Control type="tel" placeholder="Numero de telefono" name="telefono" onChange={this.props.onChange} value={this.props.formValues.telefono} />
        </Form.Group>

        <Form.Group controlId="Email">
          <Form.Label>Email</Form.Label>
          <Form.Control autoComplete="new-email" type="email" placeholder="Email" name="email" onChange={this.props.onChange} value={this.props.formValues.email} />
        </Form.Group>

        <Form.Group controlId="Password">
          <Form.Label>Password</Form.Label>
          <Form.Control autoComplete="new-password" type="password" placeholder="Password" name="password" onChange={this.props.onChange} value={this.props.formValues.password} />
        </Form.Group>

        <Form.Group controlId="Rol">
          <Form.Label>Rol</Form.Label>
          <Form.Control as="select" name="rol" onChange={this.props.onChange} value={this.props.formValues.rol}>
            <option>Almacenero</option>
            <option>Vendedor</option>
            <option>Administrador</option>
          </Form.Control>
        </Form.Group>

        <Button variant="primary" type="submit" onClick={this.handleClick}>
          Guardar
        </Button>
        {this.props.error && (
          <p className="text-danger">{this.props.error.message}</p>
        )}
      </Form>
    )
  }
}

export default FormCliente;