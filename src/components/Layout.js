import React from 'react';

import NabBar from './NabBar';
// import Footer from './Footer';
// import './styles/Layout.css'

function Layout(props){
    return(
        <React.Fragment>
          <NabBar onAuthChange={props.onAuthChange}></NabBar>
          <main role="main" className="flex-shrink-0">
            <div className="container">
              {props.children}
            </div>
          </main>
        
        </React.Fragment>
    );
}

export default Layout;