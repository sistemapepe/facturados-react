import React, { PureComponent } from 'react'

import {Link} from 'react-router-dom';
import HomeIcon from '@material-ui/icons/Home';
import PeopleIcon from '@material-ui/icons/People';
import LayersIcon from '@material-ui/icons/Layers';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import GroupIcon from '@material-ui/icons/Group';
import KeyIcon from '@material-ui/icons/VpnKey';
import ExtensionIcon from '@material-ui/icons/Extension';
import BusinessIcon from '@material-ui/icons/Business';
import AuthService from '../services/AuthService';

class SideBar extends PureComponent {
  constructor(props){
    super(props);
    this.AuthService = new AuthService();
    this.state = {open:false};
    this.menuItems=[
        {icon:<HomeIcon></HomeIcon>,link:'/'},
        {icon:<PeopleIcon></PeopleIcon>,link:'/clientes'},
        {icon:<LayersIcon></LayersIcon>,link:'/productos'},
        {icon:<ShoppingCartIcon></ShoppingCartIcon>,link:'/ventas'},
        {icon:<GroupIcon></GroupIcon>,link:'/usuarios'},
        {icon:<KeyIcon></KeyIcon>,link:'/dosificacion'},
        {icon:<ExtensionIcon></ExtensionIcon>,link:'/codcontrol'},
        {icon:<BusinessIcon></BusinessIcon>,link:'/empresa'}];
  }

  render() {
    const userType = this.AuthService.getUserAccess();
    const sections =   (userType==='Admin')?['Inicio','Clientes','Productos','Ventas','Usuarios','Dosificacion','Cod Control','Empresa']:
                        (userType==='Administrador')?['Inicio','Clientes','Productos','Ventas','Usuarios','Dosificacion','Cod Control']:
                        (userType==='Vendedor')?['Inicio','Clientes','Ventas']:['Inicio','Clientes','Productos'];
    return (
      <nav className="col-md-2 d-none d-md-block bg-light sidebar">
        <div className="sidebar-sticky">
          <ul className="nav flex-column">
            {
              sections.map((text,index) => (
                <li className="nav-item" key={text+'_link'}>
                  <Link className="nav-link" to={this.menuItems[index].link}>
                    {this.menuItems[index].icon}{text}
                  </Link>
                </li>
              ))
            }
            <li className="nav-item">
              <Link className="nav-link" to="/">
                <span data-feather="bar-chart-2"></span>
                  Reports
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/">
                <span data-feather="layers"></span>
                  Integrations
              </Link>
            </li>
          </ul>
        </div>
      </nav>
    )
  }
}

export default SideBar;