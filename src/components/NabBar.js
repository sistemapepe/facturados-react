import React, { PureComponent } from 'react'

import {Link} from 'react-router-dom';
import AuthService from '../services/AuthService';
import { Navbar, Nav} from 'react-bootstrap';

class NabBar extends PureComponent {
  constructor(props){
    super(props);
    this.AuthService = new AuthService();
    this.logout = this.logout.bind(this);
    this.state = {open:false};
    this.menuItems=[
      {link:'/'},
      {link:'/clientes'},
      {link:'/productos'},
      {link:'/ventas'},
      {link:'/usuarios'},
      {link:'/dosificacion'},
      {link:'/codcontrol'},
      {link:'/empresa'}];
  }
    
  logout(){
    this.AuthService.logout();
    this.props.onAuthChange();
  }

  render() {
    const userType = this.AuthService.getUserAccess();
    const sections =   (userType==='Admin')?[{name:'Inicio',index:0},{name:'Clientes',index:1},{name:'Productos',index:2},{name:'Ventas',index:3},{name:'Usuarios',index:4},{name:'Dosificacion',index:5},{name:'Cod Control',index:6},{name:'Empresa',index:7}]:
    (userType==='Administrador')?[{name:'Inicio',index:0},{name:'Clientes',index:1},{name:'Productos',index:2},{name:'Ventas',index:3},{name:'Usuarios',index:4},{name:'Dosificacion',index:5},{name:'Cod Control',index:6}]:
    (userType==='Vendedor')?[{name:'Inicio',index:0},{name:'Clientes',index:1},{name:'Productos',index:2},{name:'Ventas',index:3}]:[{name:'Inicio',index:0},{name:'Clientes',index:1},{name:'Productos',index:2}];
    return (
      <header>
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
          <Navbar.Brand><Link className="navbar-brand" to="/">Facturados</Link></Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
            {
              sections.map((obj) => (
                <Nav.Link key={obj.name+'_link'} as={Link} to={this.menuItems[obj.index].link}>{obj.name}</Nav.Link>
              ))
            }
            </Nav>
            <Nav>
              <Nav.Link href="/" onClick={this.logout}>Sign out</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </header>
    )
  }
}

export default NabBar