import React, { Component } from 'react';

import {Form, Button } from 'react-bootstrap';

class FormCliente extends Component {
  constructor(props) {
    super(props)
    this.state = {
            
    }
  }

  handleClick(){
    console.log('click');
  }

  render() {
    console.log("pp",this.props.formValues);
    return (
      <Form onSubmit={this.props.onSubmit} className="col-12 col-md-6 mb-5">
        <Form.Group controlId="nit">
          <Form.Label>Nit o CI</Form.Label>
          <Form.Control type="text" placeholder="Ingresa Nit" name="nit" onChange={this.props.onChange} value={this.props.formValues.nit} />
        </Form.Group>
          
        <Form.Group controlId="FullName">
          <Form.Label>Nombre</Form.Label>
          <Form.Control type="text" placeholder="Ingrese Nombre" name="nombre" onChange={this.props.onChange} value={this.props.formValues.nombre} />
        </Form.Group>

        <Button variant="primary" type="submit" onClick={this.handleClick}>
          Guardar
        </Button>
        {this.props.error && (
          <p className="text-danger">{this.props.error.message}</p>
        )}
      </Form>
    )
  }
}

export default FormCliente;