import React, { Component } from 'react';
import Requests from '../services/Requests';

import {Form, Button } from 'react-bootstrap';
import Loader from './Loader';

class FormProducto extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data:undefined,
      loading:false,
      error:null,
    }

    this.Requests = new Requests();
  }

  componentDidMount () {
    this.fetchData();
  }

  fetchData = async () => {
    this.setState({loading: true, error:null})
    try {
      const path = '/categoria/list';
      const data = await this.Requests.list(path);
      // console.log('datos',data);
      this.setState({loading: false, data:data})
    } catch(error) {
      this.setState({loading: false, error:error})
      console.log('ERR',error);
    }
  }

  handleClick(){
    console.log('click');
  }

  render() {
    if(this.state.data && this.state.data.length !==0){
      return (
        <Form onSubmit={this.props.onSubmit} className="col-12 col-md-6 mb-5">
          <Form.Group controlId="Categoria">
            <Form.Label>Categoria</Form.Label>
            <Form.Control as="select" name="categoria" onChange={this.props.onChange} value={this.props.formValues.categoria!=null?this.props.formValues.categoria._id:0}>
              <option value="0">Elegir categoria...</option>
              {this.state.data.map(articulo => {
                if(articulo.estado === 1){
                  return(<option value={articulo._id} key={articulo._id}>{articulo.nombre}</option>)
                }else {
                  return(<option value={articulo._id} key={articulo._id} disabled>{articulo.nombre}</option>)
                }
                
              })}
            </Form.Control>
          </Form.Group>
            
          <Form.Group controlId="Codigo">
            <Form.Label>Codigo</Form.Label>
            <Form.Control type="text" placeholder="Ingrese Codigo" name="codigo" onChange={this.props.onChange} value={this.props.formValues.codigo} />
          </Form.Group>

          <Form.Group controlId="Nombre">
            <Form.Label>Nombre</Form.Label>
            <Form.Control type="text" autoComplete="new-nombreprod" placeholder="Ingrese Nombre" name="nombre" onChange={this.props.onChange} value={this.props.formValues.nombre} />
          </Form.Group>

          <Form.Group controlId="Descripcion">
            <Form.Label>Descripcion</Form.Label>
            <Form.Control type="text" placeholder="Ingrese Descripcion" name="descripcion" onChange={this.props.onChange} value={this.props.formValues.descripcion} />
          </Form.Group>

          <Form.Group controlId="PrecioVenta">
            <Form.Label>Precio</Form.Label>
            <Form.Control type="number" placeholder="0.00" name="precioVenta" onChange={this.props.onChange} value={this.props.formValues.precioVenta} />
          </Form.Group>

          <Form.Group controlId="unidadMedida">
            <Form.Label>Unidad de Medida</Form.Label>
            <Form.Control as="select" name="unidadMedida" onChange={this.props.onChange} value={this.props.formValues.unidadMedida!=null?this.props.formValues.unidadMedida:0}>
              <option key="UNIDAD" value="UNIDAD">Unidad</option>
              <option key="SERVICIO" value="SERVICIO">Servicio</option>
              {/* {this.state.data.map(unidad => {
                return(<option value={unidad._id} key={unidad._id}>{unidad.nombre}</option>)
              })} */}
            </Form.Control>
          </Form.Group>

          <Form.Group controlId="Stock">
            <Form.Label>Stock</Form.Label>
            <Form.Control type="number" placeholder="0" name="stock" onChange={this.props.onChange} value={this.props.formValues.stock} />
          </Form.Group>

          <Button variant="primary" type="submit" onClick={this.handleClick}>
            Guardar
          </Button>
          
          {this.props.error && (
            <p className="text-danger">{this.props.error.message}</p>
          )}
        </Form>
      )
    } else {
      return(
        <Loader/>
      )
    }
  }
}

export default FormProducto;