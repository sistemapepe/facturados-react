import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';

import ClienteEdit from '../pages/ClienteEdit';
import UsuarioNew from '../pages/UsuarioNew';
import UsuarioEdit from '../pages/UsuarioEdit';
import ProductoNew from '../pages/ProductoNew';
import ProductoEdit from '../pages/ProductoEdit';
import CategoriaNew from '../pages/CategoriaNew';
import CategoriaEdit from '../pages/CategoriaEdit';
import DosificacionNew from '../pages/DosificacionNew';
import DosificacionEdit from '../pages/DosificacionEdit';
import Layout from './Layout';
import Home from '../pages/Home';
import Clientes from '../pages/Clientes';
import ClienteNew from '../pages/ClienteNew';
import Usuarios from '../pages/Usuarios';
import Ventas from '../pages/Ventas';
import Dosificacion from '../pages/Dosificacion';
import Empresa from '../pages/Empresa';
import Producto from '../pages/Productos'
import NotFound from '../pages/NotFound';
import Venta from '../pages/Venta';
import VentaNew from '../pages/VentaNew';

function Main(props) {
    return (
        <BrowserRouter>
          <Layout onAuthChange={props.onAuthChange}>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/clientes" component={Clientes} />
              <Route exact path="/clientes/new" component={ClienteNew} />
              <Route exact path="/clientes/:_id/edit" component={ClienteEdit} />
              <Route exact path="/usuarios" component={Usuarios} />
              <Route exact path="/usuarios/new" component={UsuarioNew} />
              <Route exact path="/usuarios/:_id/edit" component={UsuarioEdit} />
              <Route exact path="/productos" component={Producto} />
              <Route exact path="/productos/new" component={ProductoNew} />
              <Route exact path="/productos/:_id/edit" component={ProductoEdit} />
              <Route exact path="/categorias/new" component={CategoriaNew} />
              <Route exact path="/categorias/:_id/edit" component={CategoriaEdit} />
              <Route exact path="/ventas" component={Ventas} />
              <Route exact path="/ventas/new" component={VentaNew} />
              <Route exact path="/ventas/:_id" component={Venta} />
              <Route exact path="/dosificacion" component={Dosificacion} />
              <Route exact path="/dosificacion/new" component={DosificacionNew} />
              <Route exact path="/dosificacion/:_id/edit" component={DosificacionEdit} />
              <Route exact path="/empresa" component={Empresa} />

              <Route component={NotFound} />
            </Switch>
          </Layout>
        </BrowserRouter>
    )
}

export default Main;
